/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : adminsys_restaurant

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-01-25 13:50:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `xst_advise`
-- ----------------------------
DROP TABLE IF EXISTS `MP_CUST`;
CREATE TABLE `MP_CUST` (
  `ORG_CODE` varchar(32) NOT NULL,
  `CUST_CODE` varchar(32) NOT NULL,
  `CUST_NAME` varchar(128) NOT NULL,
  `SHORT_NAME` varchar(128) DEFAULT NULL,
  `LICENSE_CODE` varchar(32) DEFAULT NULL,
  `LICENSE_DATE` varchar(8) DEFAULT NULL,
  `MANAGER` varchar(128) DEFAULT NULL,
  `CUST_TEL` varchar(64) DEFAULT NULL,
  `ORDER_TEL` varchar(32) DEFAULT NULL,
  `BUSI_ADDR` varchar(256) DEFAULT NULL,
  `PERIODS` varchar(32) DEFAULT NULL,
  `PRD_ST_DATE` varchar(8) DEFAULT NULL,
  `ARR_DAY` varchar(16) DEFAULT NULL,
  `BANK_NAME` varchar(128) DEFAULT NULL,
  `ACCOUNT` varchar(64) DEFAULT NULL,
  `CUST_LMT_TYPE` varchar(32) DEFAULT NULL,
  `PAY_TYPE` varchar(32) DEFAULT NULL,
  `SALE_CENTER_CODE` varchar(32) DEFAULT NULL,
  `SALE_CENTER_NAME` varchar(64) DEFAULT NULL,
  `SALE_DEPT_CODE` varchar(32) DEFAULT NULL,
  `SALE_DEPT_NAME` varchar(64) DEFAULT NULL,
  `SLSMAN_CODE` varchar(32) DEFAULT NULL,
  `SLSMAN_NAME` varchar(64) DEFAULT NULL,
  `SLSMAN_TEL` varchar(16) DEFAULT NULL,
  `SLSMAN_MOBILE` varchar(16) DEFAULT NULL,
  `ORDER_WAY` varchar(32) DEFAULT NULL,
  `STATUS` varchar(2) DEFAULT NULL,
  `UP_TIME` date DEFAULT NULL,
  `NOTE` varchar(300) DEFAULT NULL,
  `SHORT_CODE` varchar(16) DEFAULT NULL,
  `RTL_CUST_TYPE` varchar(16) DEFAULT NULL,
  `CUST_GEO_TYPE` varchar(4) DEFAULT NULL,
  `CUST_SIZE` varchar(8) DEFAULT NULL,
  `WHSE_CODE` varchar(32) DEFAULT NULL,
  `ORDER_END_TIME` varchar(8) DEFAULT NULL,
  `EXTEND_COLUMN` varchar(8) DEFAULT NULL,
  `WEEKDAY` varchar(16) DEFAULT NULL,
  `SYNC_FLAG` char(1) DEFAULT NULL,
  `WHSE_TIME` varchar(8) DEFAULT NULL,
  `ORDER_BEGIN_TIME` varchar(8) DEFAULT NULL,
  `IS_ONLINE_PAY` char(1) DEFAULT '0',
  `DESCRIPTION` varchar(512) DEFAULT NULL,
  `REG_TIME` date DEFAULT NULL,
  `OTHER_ORDER_WAY` varchar(32) DEFAULT NULL,
  `CUST_SALEPLOY_TYPE` varchar(32) DEFAULT '0',
  `PHONE` varchar(16) DEFAULT NULL,
  `CARD_ID` varchar(32) DEFAULT NULL,
  `DOMAIN_ID` varchar(32) DEFAULT NULL,
  `DOMAIN_NAME` varchar(32) DEFAULT NULL,
  `FIRST_LETTER` varchar(1) DEFAULT NULL,
  `LICENSE_EXP_DATE` varchar(8) DEFAULT NULL,
  `LOTTERY_DATE` varchar(8) DEFAULT NULL,
  `USER_TYPE` varchar(16) DEFAULT NULL,
  `CUST_SEQ` varchar(4) DEFAULT NULL,
  `BASE_TYPE` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`ORG_CODE`,`CUST_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xst_advise`;
CREATE TABLE `xst_advise` (
  `advise_id` varchar(32) NOT NULL,
  `pic_code` varchar(2) DEFAULT NULL,
  `pic_type` varchar(2) DEFAULT NULL,
  `advise_type` varchar(2) DEFAULT NULL,
  `advise_title` varchar(128) DEFAULT NULL,
  `advise_detail` varchar(256) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shop_id` varchar(32) NOT NULL,
  `org_code` varchar(32) NOT NULL,
  `advise_state` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`advise_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_advise
-- ----------------------------
INSERT INTO `xst_advise` VALUES ('52f02e74744b49fbbe5300aa513cd2de', null, null, '1', '3333333333', '333333333333', '2018-01-25 13:41:36', '2', '1', '1');
INSERT INTO `xst_advise` VALUES ('55672d2b6ad24eb1820f77118390da6c', null, null, '0', '4343', '3434', '2018-01-25 11:43:34', '2', '1', '1');
INSERT INTO `xst_advise` VALUES ('5573a34d7a70432894ad01cae9082107', null, null, '2', '3333333333', '333333333333', '2018-01-25 13:41:39', '2', '1', '1');
INSERT INTO `xst_advise` VALUES ('623e609a2ab74d98bdaa94e4a1edf1e3', '1', '02', '1', '2323', '2322222222223', '2018-01-25 11:53:18', '232323', '1', '1');
INSERT INTO `xst_advise` VALUES ('da4e4831bde24136bf1e91dfcfc458a5', null, null, '2', '22222', '2222', '2018-01-25 13:41:43', '2', '1', '1');
INSERT INTO `xst_advise` VALUES ('da9e73e6d8154e8faa32b7c9e345edcd', null, null, '0', '啦啦啦', '啦啦啦啦', '2018-01-25 13:41:46', '34', '1', '1');
INSERT INTO `xst_advise` VALUES ('de02d5da8d5149dbae9188fff2a5b5cd', null, null, '1', '22222222222222222222', '222222222222', '2018-01-25 13:41:49', '2', '1', '1');
INSERT INTO `xst_advise` VALUES ('ffd4b9e7e9db4e55a1e27049d0c8d07a', '1', '02', '1', '222', '22', '2018-01-25 12:50:10', '22', '1', '1');

-- ----------------------------
-- Table structure for `xst_device`
-- ----------------------------
DROP TABLE IF EXISTS `xst_device`;
CREATE TABLE `xst_device` (
  `device_id` varchar(32) NOT NULL,
  `shop_id` varchar(32) NOT NULL,
  `device_name` varchar(32) DEFAULT NULL,
  `device_type` varchar(8) DEFAULT NULL,
  `device_brand` varchar(16) DEFAULT NULL,
  `device_model` varchar(16) DEFAULT NULL,
  `device_system` varchar(2) DEFAULT NULL,
  `device_scanner` varchar(2) DEFAULT NULL,
  `device_img` varchar(256) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`,`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_device
-- ----------------------------

-- ----------------------------
-- Table structure for `xst_duser`
-- ----------------------------
DROP TABLE IF EXISTS `xst_duser`;
CREATE TABLE `xst_duser` (
  `user_id` varchar(32) NOT NULL,
  `shop_id` varchar(32) NOT NULL,
  `device_id` varchar(32) NOT NULL,
  `user_name` varchar(64) NOT NULL,
  `user_age` int(4) DEFAULT NULL,
  `user_sex` varchar(4) DEFAULT NULL,
  `age_range` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`shop_id`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_duser
-- ----------------------------

-- ----------------------------
-- Table structure for `xst_img`
-- ----------------------------
DROP TABLE IF EXISTS `xst_img`;
CREATE TABLE `xst_img` (
  `img_id` varchar(32) NOT NULL,
  `shop_id` varchar(32) NOT NULL,
  `img_type` varchar(2) DEFAULT NULL,
  `device_id` varchar(32) DEFAULT NULL,
  `img_path` varchar(128) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`img_id`,`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_img
-- ----------------------------

-- ----------------------------
-- Table structure for `xst_question`
-- ----------------------------
DROP TABLE IF EXISTS `xst_question`;
CREATE TABLE `xst_question` (
  `info_id` varchar(32) NOT NULL,
  `info_type` varchar(2) DEFAULT NULL,
  `info_describe` varchar(256) DEFAULT NULL,
  `info_screen` varchar(256) DEFAULT NULL,
  `info_solve` varchar(256) DEFAULT NULL,
  `info_state` varchar(2) DEFAULT NULL,
  `is_common` varchar(2) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shop_id` varchar(32) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `org_code` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_question
-- ----------------------------
INSERT INTO `xst_question` VALUES ('019e64827c604820bcda9cff60562301', null, '烟易贷未开通', null, '帮助烟易贷开通', '1', null, '2018-01-24 16:54:32', '4', '1', '1');
INSERT INTO `xst_question` VALUES ('1', '1', '烟易贷没有开通', '343434', '555', '1', '2', '2018-01-24 15:45:41', '1', '', '1');
INSERT INTO `xst_question` VALUES ('2', '2', '全付通没绑定', '34343', '3434', '0', '2', '2018-01-25 13:38:33', '2', '1', '1');
INSERT INTO `xst_question` VALUES ('2ade119e8fb84b53967c8fb42f694fa8', '3', '走访失败', null, '走访失败', '1', null, '2018-01-25 13:38:00', '323232', '1', '1');
INSERT INTO `xst_question` VALUES ('455cb3e5f70d479c9e99196306f71033', '4', '全付通有问题', null, '处理全付通得问题', '1', null, '2018-01-25 13:38:05', '122', '1', '1');
INSERT INTO `xst_question` VALUES ('53ec3fcdfea240858f5cd595163bc64a', '', '', null, '', '1', null, '2018-01-25 11:39:47', '', '1', '1');
INSERT INTO `xst_question` VALUES ('7cd176cc509549e0b8d630b6997fbddd', '5', '全付通失效', null, '重新注册账号', '1', null, '2018-01-25 13:38:09', '2345', '1', null);
INSERT INTO `xst_question` VALUES ('7d4a489c2574418e933e573faf6c7779', null, '烟易贷未开通', null, '帮助烟易贷开通', '1', null, '2018-01-24 15:43:51', '1', '1', '2');
INSERT INTO `xst_question` VALUES ('88ed35e945494850bff22c7e75bab88c', null, '烟易贷未开通', null, '帮助烟易贷开通', '1', null, '2018-01-24 16:54:31', '3', '1', '1');
INSERT INTO `xst_question` VALUES ('f4ed00bc086f4ff89fc130c00b833e28', null, '烟易贷未开通', null, '帮助烟易贷开通', '1', null, '2018-01-24 16:54:35', '5', '1', '1');

-- ----------------------------
-- Table structure for `xst_shop`
-- ----------------------------
DROP TABLE IF EXISTS `xst_shop`;
CREATE TABLE `xst_shop` (
  `org_code` varchar(32) NOT NULL,
  `cust_code` varchar(32) NOT NULL,
  `shop_id` varchar(32) NOT NULL,
  `shop_type` varchar(32) DEFAULT NULL,
  `shop_logo` varchar(256) DEFAULT NULL,
  `shop_img` varchar(256) DEFAULT NULL,
  `shop_area` varchar(16) DEFAULT NULL,
  `shop_cabinet` varchar(8) DEFAULT NULL,
  `shop_rack` varchar(8) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_shop
-- ----------------------------

-- ----------------------------
-- Table structure for `xst_sign`
-- ----------------------------
DROP TABLE IF EXISTS `xst_sign`;
CREATE TABLE `xst_sign` (
  `pic_code` varchar(32) NOT NULL,
  `shop_id` varchar(32) NOT NULL,
  `shop_name` varchar(32) DEFAULT NULL,
  `info_id` varchar(32) NOT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pic_code`,`shop_id`,`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_sign
-- ----------------------------

-- ----------------------------
-- Table structure for `xst_tape`
-- ----------------------------
DROP TABLE IF EXISTS `xst_tape`;
CREATE TABLE `xst_tape` (
  `shop-id` varchar(32) NOT NULL,
  `tape_type` varchar(2) DEFAULT NULL,
  `crt_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`shop-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xst_tape
-- ----------------------------
